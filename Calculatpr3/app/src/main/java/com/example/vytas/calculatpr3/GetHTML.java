package com.example.vytas.calculatpr3;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class GetHTML extends Activity {

    private TextView textView;
    EditText mEdit;

    Thread thread = new Thread(){
        @Override
        public void run() {
            try {
                Thread.sleep(3500); // As I am using LENGTH_LONG in Toast
                GetHTML.this.finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEdit = (EditText) findViewById(R.id.editText);
        setContentView(R.layout.activity_get_html);
        textView = (TextView) findViewById(R.id.TextView01);

    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    private static String readStream(InputStream in) {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(in));) {

            String nextLine = "";
            while ((nextLine = reader.readLine()) != null) {
                sb.append(nextLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }


    private class DownloadWebPageTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            String readstream = "test";
            for (String url : urls) {

                try {
                    URL myURL = new URL(url);

                    HttpURLConnection client = (HttpURLConnection) myURL.openConnection();
                    readstream = readStream(client.getInputStream());




                } catch (Exception ex) {
                    System.out.println(ex);


                }

            }
            return readstream;
        }



        @Override
        protected void onPostExecute(String result){
            SharedPreferences prefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();

            editor.putString("anurl", result);
            editor.commit();
            //check it works

            if (prefs.getString("anurl", null).equals("test")){
                Toast.makeText(getApplicationContext(), "The page failed to download",
                        Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getApplicationContext(), "The page has been downloaded",
                        Toast.LENGTH_LONG).show();
            }

            thread.start();
        }
    }

    public void readWebpage(View view) {
        DownloadWebPageTask task = new DownloadWebPageTask();
        EditText edit = (EditText) findViewById(R.id.editText);
        task.execute(new String[]{edit.getText().toString()});
    }
}
