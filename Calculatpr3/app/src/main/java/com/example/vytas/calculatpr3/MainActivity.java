package com.example.vytas.calculatpr3;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    String total = "";
    double v1, v2;
    String sign = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        double PrevResult = 0;

        SharedPreferences prefs = getSharedPreferences("MYPREF2", Context.MODE_PRIVATE);
        PrevResult = (double) prefs.getFloat("Result_file", 0);

        setContentView(R.layout.activity_main);
        EditText edit = (EditText) findViewById(R.id.editText);
        edit.setText(Double.toString(PrevResult));
        total = Double.toString(PrevResult);


    }



    public void onClick(View v) {
        Button button = (Button) v;
        String str = button.getText().toString();
        total += str;
        EditText edit = (EditText) findViewById(R.id.editText);
        edit.setText(total);
        System.out.println("click: " + "Total: " + total + " v1: " + v1 + " v2: " + v2);

    }

    public void onAdd(View v) {
        v1 = Double.parseDouble(total);
        total = "";
        Button button = (Button) v;
        String str = button.getText().toString();
        sign = str;
        EditText edit = (EditText) findViewById(R.id.editText);
        edit.setText("");
        System.out.println("add: " + "Total: " + total + " v1: " + v1 + " v2: " + v2);

    }

    public void onCalculate(View v) {
        EditText edit = (EditText) findViewById(R.id.editText);
        String str2 = edit.getText().toString();
        v2 = Double.parseDouble(str2);
        double grand_total = 0;
        if (sign.equals("+")) {
            grand_total = v1 + v2;
        }
        if (sign.equals("-")) {
            grand_total = v1 - v2;
        }
        if (sign.equals("*")) {
            grand_total = v1 * v2;
        }
        if (sign.equals("/")) {
            grand_total = v1 / v2;
        }

        try {
            SharedPreferences prefs = getSharedPreferences("MYPREF2", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putFloat("Result_file", (float) grand_total);
            editor.commit();
            System.out.println(prefs.getFloat("Result_file", 0));

        } catch (Exception ex) {

        }
        System.out.println("calculate: " + "Total: " + total + " v1: " + v1 + " v2: " + v2);
        edit.setText(grand_total + "");
        total = Double.toString(grand_total);
        Intent intent = new Intent(this, GetHTML.class);
        startActivity(intent);

    }

    public void onClear(View v) {
        EditText edit = (EditText) findViewById(R.id.editText);
        edit.setText("");
        total = "";
        try {
            SharedPreferences prefs = getSharedPreferences("MYPREF2", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putFloat("Result_file", 0);
            editor.commit();
            System.out.println(prefs.getFloat("Result_file", 0));

        } catch (Exception ex) {

        }
    }
}
